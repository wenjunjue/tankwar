﻿using UnityEngine;
using System;

public class CameraControl : MonoBehaviour
{
    public Transform Player1;    //得到第一辆坦克
    public Transform Player2;    //得到第二辆坦克

    private Vector3 offset;      //偏移
    private Camera camera;
    void Start()
    {
        
        offset = transform.position - (Player1.position + Player2.position) / 2;
        camera = this.GetComponent<Camera>();
    }
   void Update()
    {
        //坦克被销毁后不跟随
        if (Player1 == null || Player2 == null) return;
        //视角保持在两辆坦克的中心
        transform.position = (Player1.position + Player2.position) / 2 + offset;
        //计算两辆坦克的距离
        float distance = Vector3.Distance(Player1.position, Player2.position);
        //坦克越近视角越小，坦克越远视角越大
        float size = distance * 0.6f;
        camera.orthographicSize = size;
    }
}