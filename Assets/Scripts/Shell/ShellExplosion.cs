﻿using UnityEngine;

public class ShellExplosion : MonoBehaviour
{
    public GameObject shellExplosionPrefab;      //得到爆炸特效预制体


    public AudioClip shellExplosionAudio;        //得到子弹爆炸音效


 
    private void OnTriggerEnter(Collider collider)
    {
        //子弹爆炸时播放音效
        AudioSource.PlayClipAtPoint(shellExplosionAudio, transform.position);
        //设置爆炸预制体生成的位置
        GameObject.Instantiate(shellExplosionPrefab, transform.position, transform.rotation);
        //当爆炸产生是销毁该自身
        GameObject.Destroy(this.gameObject);

        if (collider.tag=="Tank")//检测碰撞的是不是Tank
        {
            collider.SendMessage("TakeDamage");//如果碰撞的是Tank发送一个消息
        }

    }
}