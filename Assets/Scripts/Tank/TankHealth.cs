﻿using UnityEngine;
using UnityEngine.UI;


public class TankHealth : MonoBehaviour
{
    public int hp = 100;                //定义初始血量
    public GameObject tankExplosion;    //获取坦克爆炸的特效
    public AudioClip tankExPlosionAudio; //获取坦克爆炸的声音组件
    public Slider hpSlider;              //获取血条组件

    private int hpTotal;             //总血量
     void Start()
    {
        hpTotal = hp;
    }

    void TakeDamage() {

        if (hp <= 0) return;
        hp -= Random.Range(10, 20);    //设置子弹击中坦克时的伤害，每次击中的伤害为随机数，取值范围是10至20

        hpSlider.value = (float)hp / hpTotal;  //红色血条跟着血量减少而减少

        if (hp<=0)
        {    //播放爆炸声音
            AudioSource.PlayClipAtPoint(tankExPlosionAudio,transform.position);
            //如果血量变为0，播放爆炸特效
            GameObject.Instantiate(tankExplosion, transform.position + Vector3.up, transform.rotation);
             //如果血量变为0，销毁游戏对象
            GameObject.Destroy(this.gameObject);
        }
    }
}