﻿using UnityEngine;

public class TankMovement : MonoBehaviour
{
    public float speed = 10;                //坦克前后移动的速度
    public float angularSpeed = 5;          //坦克每秒原地旋转的角度
    public float number = 1;                //坦克的编号，用于区分玩家1和玩家2
    public AudioClip idleAudio;             //获取停车时的引擎声
    public AudioClip drivingAudio;          //获取开车时的引擎声

    private AudioSource audio;               //获取音频组件
    private Rigidbody rigidbody;             //坦克刚体组件
    void Start()
    {
        rigidbody = this.GetComponent<Rigidbody>();  //获取刚体组件
        audio = this.GetComponent<AudioSource>();

    }
    void FixedUpdate()
    {
        float v = Input.GetAxis("Vertical" + number);                 //获取前后按键
        rigidbody.velocity = transform.forward * v * speed;         //给刚体施力让坦克移动起来

        float h = Input.GetAxis("Horizontal" + number);             //获取左右按键
        rigidbody.angularVelocity = transform.up * h * angularSpeed;       //给刚体施加力让坦克旋转起来
        //判断停车和开车，分别播放不同的音效
        if (Mathf.Abs(h) > 0.1 || Mathf.Abs(v) > 0.1)
        {
            audio.clip = drivingAudio;
            if (audio.isPlaying == false)
                audio.Play();
        }
        else
        {
            audio.clip = idleAudio;
            if (audio.isPlaying == false)
                audio.Play();
        }
    }
}