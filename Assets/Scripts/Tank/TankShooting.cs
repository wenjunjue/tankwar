﻿using UnityEngine;
using UnityEngine.UI;

public class TankShooting : MonoBehaviour
{
    public GameObject shellPrefab;             //得到子弹预制体
    public KeyCode fireKey = KeyCode.Space;    //设置空格为发射按钮
    public float shellSpeed = 10;              //设置子弹初始飞行速度
    public AudioClip shotAudio;                //获取子弹发射音效


    private Transform firePositon;             //得到子弹发射初始位置

     void Start()
    {
        firePositon = transform.Find("FirePosition");//得到子弹发射初始位置
    }
     void Update()
    {
        if (Input.GetKeyDown(fireKey))
        {
            //发射子弹时播放音效
            AudioSource.PlayClipAtPoint(shotAudio,transform.position);
            //在初始位置生成子弹
            GameObject go=GameObject.Instantiate(shellPrefab, firePositon.position, firePositon.rotation)as GameObject;
            //得到子弹刚体组件并给它一个速度
            go.GetComponent<Rigidbody>().velocity = go.transform.forward * shellSpeed;
        }
    }
}